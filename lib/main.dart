import 'package:flutter/material.dart';
import 'package:note_app/controller/google_auth.dart';
import 'package:note_app/ui/HomeScreen/home_screen.dart';
import 'package:note_app/ui/LoginScreen/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String token = getToken().toString();
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: (token == null) ? LoginScreen() : HomePage(),
    );
  }
}
