import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'addnote_state.dart';

class AddNoteCubit extends Cubit<AddNoteState> {
  CollectionReference ref = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser?.uid)
      .collection('notes');
  TextEditingController title = TextEditingController();
  TextEditingController des = TextEditingController();
  AddNoteCubit() : super(AddNoteState());
  void add(context) async {
    var data = {
      'title': title.text,
      'description': des.text,
      'created': DateTime.now(),
      'lastupdate': DateTime.now(),
    };

    ref.add(data);

    Navigator.pop(context);
  }
}
