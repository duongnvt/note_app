import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

Widget NoteCard (
    String? title, String? des, DateTime? created, DateTime? lastupdate){
  String createdTime =
  DateFormat.yMMMd().add_jm().format(created!);
  String lastupdateTime =
  DateFormat.yMMMd().add_jm().format(lastupdate!);
  return Card(
    color: Colors.yellow,
    child: Padding(
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          Text(
            "$des",
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.black,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          (created == lastupdate)
              ? Container(
            alignment: Alignment.centerRight,
            child: Text(
              ("Created: $createdTime"),
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.grey,
              ),
            ),
          )
              : Container(
            alignment: Alignment.centerRight,
            child: Text(
              ("Last Update: $lastupdateTime"),
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}