import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
part 'viewnote_state.dart';

class ViewNoteCubit extends Cubit<ViewNoteState> {
  ViewNoteCubit() : super(ViewNoteState());
  void init(String title, String des){
    emit(state.copyWith(title: title,des: des));
  }
  void save(DocumentReference? ref, BuildContext context) async {
    if (state.edit == true) {
      await ref?.update({
        'title': state.title,
        'description': state.des,
        'lastupdate': DateTime.now()
      });
    }
    Navigator.pop(context);
  }

  void delete(DocumentReference? ref, BuildContext context) async {
    await ref?.delete();
    Navigator.pop(context);
  }

  void onChangeTitle(String value) {
    emit(state.copyWith(title: value));
    emit(state.copyWith(edit: true));
  }

  void onChangeDes(String value) {
    emit(state.copyWith(des: value));
    emit(state.copyWith(edit: true));
  }
}
