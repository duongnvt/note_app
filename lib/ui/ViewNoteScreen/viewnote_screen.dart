import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/ui/ViewNoteScreen/viewnote_cubit.dart';

class ViewNotePage extends StatelessWidget {
  final Map? data;
  final DocumentReference? ref;
  const ViewNotePage({Key? key, this.data, this.ref})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return ViewNoteCubit();
      },
      child: ViewNoteScreen(
        data: data,
        ref: ref,
      ),
    );
  }
}

class ViewNoteScreen extends StatefulWidget {
  final Map? data;
  final DocumentReference? ref;
  const ViewNoteScreen({Key? key, this.data, this.ref})
      : super(key: key);
  @override
  State<ViewNoteScreen> createState() => _ViewNoteScreenState();
}

class _ViewNoteScreenState extends State<ViewNoteScreen> {

  ViewNoteCubit? _cubit;

  @override
  void initState() {
    _cubit = context.read<ViewNoteCubit>();
    super.initState();
    _cubit?.init(widget.data?['title'], widget.data?['description']);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_outlined),
          onPressed: () {
            _cubit?.save(widget.ref, context);
          },
        ),
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () {
                _cubit?.delete(widget.ref, context);
              },
              icon: Icon(Icons.delete)),
        ],
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              TextFormField(
                initialValue: widget.data?['title'],
                maxLines: null,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration.collapsed(
                  hintText: "Title",
                ),
                style: TextStyle(
                  fontSize: 35.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
                onChanged: (val) {
                  _cubit?.onChangeTitle(val);
                },
              ),
              Expanded(
                child: TextFormField(
                  initialValue: widget.data?['description'],
                  maxLines: null,
                  keyboardType: TextInputType.multiline,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    hintText: "Note Description",
                  ),
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                  onChanged: (val) {
                    _cubit?.onChangeDes(val);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
