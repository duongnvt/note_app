part of 'viewnote_cubit.dart';

@immutable
class ViewNoteState extends Equatable {
  String? title;
  String? des;
  bool edit;
  ViewNoteState({this.title, this.des, this.edit = false});

  ViewNoteState copyWith({
    String? title,
    String? des,
    bool? edit,
  }) {
    return ViewNoteState(
      title: title ?? this.title,
      des: des ?? this.des,
      edit: edit ?? this.edit,
    );
  }
  @override
  List<Object?> get props => [
    this.title,
    this.des,
    this.edit,
  ];
}
