import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:note_app/controller/google_auth.dart';
import 'package:note_app/ui/AddNoteScreen/addnote_screen.dart';
import 'package:note_app/ui/HomeScreen/home_cubit.dart';
import 'package:note_app/ui/LoginScreen/login_screen.dart';
import 'package:note_app/ui/ViewNoteScreen/viewnote_screen.dart';
import 'package:note_app/ui/note_card.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        return HomeCubit();
      },
      child: HomeScreen(),
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeCubit? _cubit;

  @override
  void initState() {
    _cubit = context.read<HomeCubit>();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: BlocBuilder<HomeCubit, HomeState>(
          builder: (context, state) {
            return IconButton(
              icon: Icon(Icons.logout),
              onPressed: () {
                logOut();
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                );
              },
            );
          },
        ),
        title: Text('Note App'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return AddNotePage();
              })).then((value) {
                setState(() {});
              });
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: SafeArea(
          child: Container(
            margin: EdgeInsets.all(10),
        child: FutureBuilder<QuerySnapshot>(
          future: _cubit?.ref.get(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data?.docs.length == 0) {
                return Center(
                  child: Text(
                    "You have no saved Notes !",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                );
              }
              return ListView.builder(
                itemCount: snapshot.data?.docs.length,
                itemBuilder: (context, index) {
                  Map data = snapshot.data?.docs[index].data() as Map;
                  DateTime created = data['created']?.toDate();
                  DateTime lastupdate = data['lastupdate']?.toDate();
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .push(
                            MaterialPageRoute(
                              builder: (context) => ViewNotePage(
                                data: data,
                                ref: snapshot.data?.docs[index].reference,
                              ),
                            ),
                          )
                              .then((value) {
                            setState(() {});
                          });
                        },
                        child: NoteCard(data['title'], data['description'], created, lastupdate)
                      ),
                      SizedBox(height: 10,)
                    ],
                  );
                },
              );
            } else {
              return Center(
                child: Text("Loading..."),
              );
            }
          },
        ),
      )),
    );
  }
}
